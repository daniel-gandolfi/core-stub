## Install
``` npm install ```

## Deploy
``` npm start ```

## Troubleshooting
If port 899 is not available to use, use the port 1899 and then redirect internet traffic 1899 -> 899 using these commands:
```
sudo iptables -t nat -I PREROUTING --src 0/0 --dst 127.0.0.1 -p tcp --dport 899 -j REDIRECT --to-ports 1899
sudo iptables -t nat -I OUTPUT --src 0/0 --dst 127.0.0.1 -p tcp --dport 899 -j REDIRECT --to-ports 1899
```
