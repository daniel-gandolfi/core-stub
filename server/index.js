const express = require("express")
const app = express()
const xmlJs = require("xml-js")

let stateXml = require("fs").readFileSync(require("path").join(__dirname, "stato.xml"), "utf-8")

app.use(express.static("public"))

app.use(require("body-parser").json())

app.post("/stato.xml", async (req, res) => {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Methods", "POST")
    res.setHeader("Access-Control-Max-Age", "1000")
    res.setHeader("Content-Type", "text/html")
    res.setHeader("Connection", "close")

    res.send(stateXml)
})

app.post("/change-state", async (req, res) => {
    stateXml = getStateXml({
        loyaltyCode: req.body.loyaltyCode,
        credit: req.body.credit,
        state: req.body.state,
        bonus: req.body.bonus
    })

    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Methods", "POST")
    res.setHeader("Access-Control-Max-Age", "1000")

    res.sendStatus(200)
})

app.listen(899, () => console.log("Listening on port 899"))

function getStateXml(data) {
    const origData = stateXml

    if (data.loyaltyCode != null) {
        return getStateXmlModLoyaltyCode(data.loyaltyCode)
    } else if (data.credit != null) {
        return getStateXmlModCredit(data.credit)
    } else if (data.state != null) {
        return getStateXmlModState(data.state)
    } else {
        return origData;
    }
}

function getStateXmlModCredit(credit) {
    return stateXml.replace(/<Credito>(.+?)<\/Credito>/, `<Credito>${credit}</Credito>`)
}

function getStateXmlModState(state) {
    const states = {
        AVVIO_IN_CORSO: "AvvioInCorso",
        RIPRISTINO_IN_CORSO: "RipristinoInCorso",
        PRONTO: "Pronto",
        PROBLEMA_HW: "ProblemaHW",
        RESTITUZIONE_CREDITO_IN_CORSO: "RestituzioneCreditoInCorso",
        RESTITUZIONE_CREDITO_COMPLETATA: "RestituzioneCreditoCompletata",
        RESTITUZIONE_CREDITO_FALLITA: "RestituzioneCreditoFallita",
        VENDITA_IN_CORSO: "VenditaInCorso",
        VENDITA_COMPLETATA: "VenditaCompletata",
        VENDITA_FALLITA: "VenditaFallita",
        VERIFICA_IN_CORSO: "VerificaInCorso",
        VOUCHER_VALIDO: "VoucherValido",
        VOUCHER_NON_VALIDO: "VoucherNonValido",
        BONUS_LIMITI_VARIATI: "BonusLimitiVariati",
        HTTP_ERROR: "HttpError"
    }

    const json = JSON.parse(xmlJs.xml2json(stateXml, { compact: true }))

    const statoTgNode = json.StatoRisposta.StatoTG
    statoTgNode._attributes.Id = states[state]

    return xmlJs.json2xml(JSON.stringify(json), { compact: true, spaces: 2 })
}

function getStateXmlModLoyaltyCode(loyaltyCode) {
    const json = JSON.parse(xmlJs.xml2json(stateXml, { compact: true }))

    if (loyaltyCode == "") {
        delete json.StatoRisposta.Loyalty
    } else {
        json.StatoRisposta.Loyalty = { _text: loyaltyCode }
    }

    return xmlJs.json2xml(JSON.stringify(json), { compact: true, spaces: 2 })
}