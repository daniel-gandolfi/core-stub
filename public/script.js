const states = {
    AVVIO_IN_CORSO: "AvvioInCorso",
    RIPRISTINO_IN_CORSO: "RipristinoInCorso",
    PRONTO: "Pronto",
    PROBLEMA_HW: "ProblemaHW",
    RESTITUZIONE_CREDITO_IN_CORSO: "RestituzioneCreditoInCorso",
    RESTITUZIONE_CREDITO_COMPLETATA: "RestituzioneCreditoCompletata",
    RESTITUZIONE_CREDITO_FALLITA: "RestituzioneCreditoFallita",
    VENDITA_IN_CORSO: "VenditaInCorso",
    VENDITA_COMPLETATA: "VenditaCompletata",
    VENDITA_FALLITA: "VenditaFallita",
    VERIFICA_IN_CORSO: "VerificaInCorso",
    VOUCHER_VALIDO: "VoucherValido",
    VOUCHER_NON_VALIDO: "VoucherNonValido",
    BONUS_LIMITI_VARIATI: "BonusLimitiVariati",
    HTTP_ERROR: "HttpError"
}

const serverBaseUrl = "http://localhost:899"

Vue.component("states", {
    data: function() {
        return {
            currentState: "PRONTO",
            states: Object.entries(states).map(el => ({ value: el[0], text: el[1] }))
        }
    },
    template: `
        <div>
            <label for="state">Stato</label>
            <select v-model="currentState" id="state" @change="postData('state', currentState)">
                <option v-for="option in states" :value="option.value">
                    {{ option.text }}
                </option>
            </select>
        </div>
    `,
    methods: {
        postData
    }
})

Vue.component("loyalty-code", {
    data: function() {
        return {
            loyaltyCode: "",
            timeout: 0
        }
    },
    methods: {
        postToServerDebounced(value) {
            if(this.timeout) clearTimeout(this.timeout);

            var that=this;

            this.timeout = setTimeout(() => {
                that.loyaltyCode = value;
                postData("loyaltyCode", that.loyaltyCode)
            }, 5000);
        }
    },
    template: `
        <div>
            <label for="loyaltyCode">Codice Loyalty</label>
            <input :value="loyaltyCode" @input="postToServerDebounced($event.target.value)" id="loyaltyCode" type="text">
        </div>
    `,

})

Vue.component("credit", {
    data: function() {
        return { credit: 0 }
    },
    template: `
        <div>
            <label for="credit">Credito</label>
            <input v-model="credit" @input="postData('credit', credit)" id="credit" type="number" step="any">
        </div>
    `,
    methods: {
        postData
    }
})

new Vue({ el: "#app" })

function postData(key, value) {
    return fetch(`${serverBaseUrl}/change-state`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors',
        body: JSON.stringify({ [key]: value })
    })
}
