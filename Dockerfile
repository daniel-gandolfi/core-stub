FROM node:13-alpine
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm i
COPY . .
EXPOSE 899
CMD [ "npm", "start" ]
